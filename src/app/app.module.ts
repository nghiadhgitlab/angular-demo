import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MatButtonModule,
          MatCheckboxModule,
          MatToolbarModule,
          MatIconModule,
          MatMenuModule,
          MatFormFieldModule,
          MatOptionModule,
          MatSelectModule,
          MatInputModule,
          MatProgressBarModule } from '@angular/material';
import { HeaderComponent } from './component/header/header.component';
import { RegisterComponent } from './component/register/register.component';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { InfluencerRegisterComponent } from './component/influencer-register/influencer-register.component';
import { MaterialFileUploadComponent } from './component/material-file-upload/material-file-upload.component';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { influencerRegisterReducer } from './ngrx/reducer/influencer-register.reducer';
import { InfluencerRegistertEffects } from './ngrx/effect/influencer-register.effect';
import { RegisterService } from './service/register-service.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const MATERIAL_COMPONENTS = [
  MatButtonModule,
  MatToolbarModule,
  MatIconModule,
  MatCheckboxModule,
  MatMenuModule,
  MatFormFieldModule,
  MatOptionModule,
  MatSelectModule,
  MatInputModule,
  MatProgressBarModule
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    RegisterComponent,
    InfluencerRegisterComponent,
    MaterialFileUploadComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MATERIAL_COMPONENTS,
    StoreModule.forRoot({ influencerRegister: influencerRegisterReducer }),
    EffectsModule.forRoot([InfluencerRegistertEffects]),
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    MATERIAL_COMPONENTS
  ],
  providers: [
    RegisterService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
