import { Action } from '@ngrx/store';

export enum InfluencerRegisterActionType {
  REGISTER = '[Influencer] Influencer Register',
  REGISTER_SUCCESS = '[Influencer] Influencer Register Success',
  REGISTER_FAIL = '[Influencer] Influencer Register Fail',
}

export class InfluencerRegister implements Action {
  readonly type = InfluencerRegisterActionType.REGISTER;
  constructor(public payload: string) {}
}

export class InfluencerRegisterSuccess implements Action {
  readonly type = InfluencerRegisterActionType.REGISTER_SUCCESS;
  constructor(public payload: string[]) {}
}

export class InfluencerRegisterFail implements Action {
  readonly type = InfluencerRegisterActionType.REGISTER_FAIL;
  constructor(public payload: string, public error: any) {}
}

export type Actions = InfluencerRegister | InfluencerRegisterSuccess | InfluencerRegisterFail;
