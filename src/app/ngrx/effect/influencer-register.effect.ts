import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { Action } from '@ngrx/store';
import { map, catchError, switchMap, concatMap } from 'rxjs/operators';
import { of } from 'rxjs';
import * as actions from '../action/influencer-register.action';
import { RegisterService } from 'src/app/service/register-service.service';

@Injectable()
export class InfluencerRegistertEffects {
  constructor(private service: RegisterService, private actions$: Actions) {}

  @Effect()
  loadCasesOrgExport$: Observable<Action> = this.actions$.pipe(
    ofType<actions.InfluencerRegister>(actions.InfluencerRegisterActionType.REGISTER),
    concatMap(action => {
      return this.service.register(action.payload).pipe(
        map((res: any) => {
          return new actions.InfluencerRegisterSuccess(res);
        }),
        catchError(error => of(new actions.InfluencerRegisterFail(action.payload, error)))
      );
    })
  );
}
