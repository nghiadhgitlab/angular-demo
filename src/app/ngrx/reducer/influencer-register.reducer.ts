import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { Actions, InfluencerRegisterActionType } from '../action/influencer-register.action';
/**
 * @ngrx/entity provides a predefined interface for handling
 * a structured dictionary of records. This interface
 * includes an array of ids, and a dictionary of the provided
 * model type by id. This interface is extended to include
 * any additional interface properties.
 */
export interface State extends EntityState<any> {
  selectedId: number | null;
  loading: boolean;
  loaded: boolean;
}

/**
 * createEntityAdapter creates many an object of helper
 * functions for single or multiple operations
 * against the dictionary of records. The configuration
 * object takes a record id selector function and
 * a sortComparer option which is set to a compare
 * function if the records are to be sorted.
 */
export const adapter: EntityAdapter<any> = createEntityAdapter<any>();

/** getInitialState returns the default initial state
 * for the generated entity state. Initial state
 * additional properties can also be defined.
 */
export const initialState: State = adapter.getInitialState({
  selectedId: null,
  loading: false,
  loaded: false,
});

export function influencerRegisterReducer(state = initialState, action: Actions): State {
  switch (action.type) {
    case InfluencerRegisterActionType.REGISTER:
      return {
        ...state,
        loading: true,
        loaded: false,
      };

    case InfluencerRegisterActionType.REGISTER_SUCCESS:
      return {
        ...adapter.addOne(action.payload, state),
        loading: false,
        loaded: true,
      };
    case InfluencerRegisterActionType.REGISTER_FAIL:
      return {
        ...state,
        loading: false,
        loaded: true,
      };
    default: {
      return state;
    }
  }
}
