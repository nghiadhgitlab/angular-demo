import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfluencerRegisterComponent } from './influencer-register.component';

describe('InfluencerRegisterComponent', () => {
  let component: InfluencerRegisterComponent;
  let fixture: ComponentFixture<InfluencerRegisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfluencerRegisterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfluencerRegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
