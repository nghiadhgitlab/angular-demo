import { Component, OnInit } from '@angular/core';
import * as action from '../../ngrx/action/influencer-register.action';
import { Store } from '@ngrx/store';
import * as influencerRegisterReducer from '../../ngrx/reducer/influencer-register.reducer';
import { Subscription } from 'rxjs';
import { Actions, ofType } from '@ngrx/effects';
import { RegisterService } from 'src/app/service/register-service.service';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-influencer-register',
  templateUrl: './influencer-register.component.html',
  styleUrls: ['./influencer-register.component.scss']
})
export class InfluencerRegisterComponent implements OnInit {

  registerSuccessSub: Subscription;
  form: FormGroup;
  avatarServerRes: any;

  constructor(private store: Store<influencerRegisterReducer.State>,
              private dispatcher: Actions,
              private formBuilder: FormBuilder,
              private reigsterService: RegisterService) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      name: [''],
      bankNo: [''],
      idNo: [''],
      avatar: ['']
    });
    this.subscribe();
  }

  subscribe() {
    this.registerSuccessSub = this.dispatcher.pipe(
      ofType<action.InfluencerRegister>(action.InfluencerRegisterActionType.REGISTER_SUCCESS)).subscribe(action => {
        this.reigsterService.setData({
          name: this.form.controls['name'].value,
          avatar: this.avatarServerRes.link
        });
    });
  }

  onFileComplete(data: any) {
    this.avatarServerRes = data;
  }
  submit() {
    this.store.dispatch(new action.InfluencerRegister('Register'));
  }

}
