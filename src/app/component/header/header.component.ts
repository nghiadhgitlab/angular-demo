import { Component, OnInit, OnDestroy } from '@angular/core';
import { RegisterService } from 'src/app/service/register-service.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {

  subscription: Subscription;
  avatarLink: string;
  name: string;

  constructor(private reigsterService: RegisterService, private router: Router) {
    this.subscription = this.reigsterService.getData()
    .subscribe(data => {
      this.avatarLink = data.avatar;
      this.name = data.name;
    });
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  logoClick() {
    this.router.navigate(['/']);
  }

}
