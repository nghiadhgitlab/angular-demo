import { Component, OnInit } from '@angular/core';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
declare var FB: any;

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  isInfluencer = false;

  constructor(private iconRegistry: MatIconRegistry, private sanitizer: DomSanitizer, private router: Router) {
    this.iconRegistry.addSvgIcon(
        'facebook',
        this.sanitizer.bypassSecurityTrustResourceUrl('assets/img/facebook.svg'));
     this.iconRegistry.addSvgIcon(
        'instagram',
        this.sanitizer.bypassSecurityTrustResourceUrl('assets/img/instagram.svg'));
  }

  ngOnInit() {
    (window as any).fbAsyncInit = function() {
      FB.init({
        appId      : '646865375800854',
        cookie     : true,
        xfbml      : true,
        version    : 'v3.1'
      });
      FB.AppEvents.logPageView();
    };

    (function(d, s, id) {
       let js, fjs = d.getElementsByTagName(s)[0];
       if (d.getElementById(id)) {
         return;
        }
       js = d.createElement(s); js.id = id;
       js.src = 'https://connect.facebook.net/en_US/sdk.js';
       fjs.parentNode.insertBefore(js, fjs);
     }(document, 'script', 'facebook-jssdk'));
  }

  loginWithFacebook() {
    FB.login((response) => {
          if (response.authResponse) {
            this.router.navigate(['/influencerRegister']);
           } else {
           console.log('User login failed');
         }
      });
  }

  influencerRegister() {
    this.isInfluencer = true;
  }

  brandRegister() {
  }

}
