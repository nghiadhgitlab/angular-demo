import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Subject, Observable } from 'rxjs';

@Injectable()
export class RegisterService {

  private data = new Subject<any>();

  constructor(private http: HttpClient) {}

  register(payload: any) {

    return this.http.post(`http://59c09256a101d20011afd606.mockapi.io/testform/test`, payload).pipe(
      map((res: any) => {
        return res;
      })
    );
  }

  getData(): Observable<any> {
    return this.data.asObservable();
  }

  setData(data: any) {
    this.data.next(data);
  }

}
