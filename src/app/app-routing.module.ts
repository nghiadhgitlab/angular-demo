import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterComponent } from './component/register/register.component';
import { InfluencerRegisterComponent } from './component/influencer-register/influencer-register.component';


const routes: Routes = [
  {
    path: '',
    component: RegisterComponent
  },
  {
    path: 'influencerRegister',
    component: InfluencerRegisterComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
